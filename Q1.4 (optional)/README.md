Sometimes views designed to receive tap/click events failed to respond. Please write a debug tool to find out the reason(s).

1. You should think about how to trigger this debug tool.

2. The diagnostic output may not be a simple result, try your best and make it developer friendly. 

3. a Readme file to explain your plan.

4. Android required; iOS optionnnal.
