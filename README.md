# Instructions #

Welcome to the programming challenge from Maypole.

You will find the questions in the **sub-directories**. You can answer them by adding **new files or folders**. Please treat each question as a ticket and make the corresponding **git commits** the way you would normally do.

After you finish, run the following command and send the submission file to jobs@maypole.tv with your contact info.

```bash
git bundle create ../submission.bundle --all
```

Thank you! Let's get started!